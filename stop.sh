#!/bin/bash

# Check for Docker Compose
if ! command -v docker-compose &> /dev/null; then
    echo "Docker Compose could not be found. Please install it."
    echo "Installation URL: https://docs.docker.com/compose/install/"
    exit
fi

# Stop the Docker Compose services
echo "Stopping Hangman Docker Compose services..."
docker-compose -f docker-compose.yml down

echo "Services have been stopped."
