#!/bin/bash

# Check for Docker Compose
if ! command -v docker-compose &> /dev/null; then
    echo "Docker Compose could not be found. Please install it."
    echo "Installation URL: https://docs.docker.com/compose/install/"
    exit
fi

# Stop existing services if running
docker-compose -f docker-compose.yml down

# Start services
docker-compose -f docker-compose.yml up -d --build

# Check the environment and open a browser window
UNAME=$(uname)
if [[ "$UNAME" == 'Darwin' ]]; then
    # macOS
    open http://localhost:4000 || echo 'Could not open browser. Visit http://localhost:4000 manually.'
elif [[ "$UNAME" == 'Linux' ]]; then
    # Check if it's WSL
    if grep -qEi 'microsoft|WSL' /proc/version; then
        # WSL environment
        cmd.exe /C start http://localhost:4000 || echo 'Could not open browser. Visit http://localhost:4000 manually.'
    else
        # Regular Linux
        xdg-open http://localhost:4000 || echo 'Could not open browser. Visit http://localhost:4000 manually.'
    fi
else
    # Assuming Windows but not WSL
    cmd.exe /C start http://localhost:4000 || echo 'Could not open browser. Visit http://localhost:4000 manually.'
fi
