"""Unit tests for the HangmanGame class and the Hangman API."""

import pytest
from src.logic.hangman import HangmanGame
from src.api.hangman_api import app as api_app
from src.logic.hangman import INVALID_GUESS, GAME_NOT_IN_PROGRESS, LETTER_ALREADY_GUESSED


@pytest.fixture
def game_fixture() -> HangmanGame:
    """Create a new HangmanGame instance for testing.

    :return: A new instance of HangmanGame.
    :rtype: HangmanGame
    """
    return HangmanGame()


@pytest.fixture
def api_client_fixture():
    """Create a Flask test client for API testing.

    :yield: A Flask test client.
    """
    api_app.config['TESTING'] = True
    with api_app.test_client() as client:
        yield client


def test_start_game(game_fixture: HangmanGame):  # pylint: disable=redefined-outer-name
    """Test the start_game method for initializing a new game.

    :param HangmanGame game_fixture: An instance of the HangmanGame for testing.
    """
    game_fixture.start_game('easy')
    assert game_fixture.status == 'in_progress'
    assert game_fixture.current_phrase != ''


def test_start_game_medium_difficulty(game_fixture):
    """Test starting a game with 'medium' difficulty."""
    game_fixture.start_game('medium')
    phrase_length = len(game_fixture.current_phrase)
    assert 15 <= phrase_length < 25


def test_start_game_hard_difficulty(game_fixture):
    """Test starting a game with 'hard' difficulty."""
    game_fixture.start_game('hard')
    phrase_length = len(game_fixture.current_phrase)
    assert 25 <= phrase_length < 100


def test_guess_when_game_not_in_progress(game_fixture):
    """Test making a guess when the game is not in progress."""
    # Test before starting the game
    response = game_fixture.make_guess('A')
    assert response == GAME_NOT_IN_PROGRESS


def test_make_guess(game_fixture: HangmanGame):  # pylint: disable=redefined-outer-name
    """Test the make_guess method for valid and invalid guesses.

    :param HangmanGame game_fixture: An instance of the HangmanGame for testing.
    """
    game_fixture.start_game('easy')
    result = game_fixture.make_guess('A')
    assert result is None or result in [
        'Game not in progress.',
        'Invalid guess. Please enter a single letter.',
        'Letter already guessed.',
        'Guess accepted']
    for letter in set(filter(str.isalpha, game_fixture.current_phrase)):
        game_fixture.make_guess(letter)
    assert game_fixture.get_game_status() == 'won'


def test_special_characters_visible(game_fixture: HangmanGame):  # pylint: disable=redefined-outer-name
    """Test if special characters are visible in the masked phrase.

    :param HangmanGame game_fixture: An instance of the HangmanGame for testing.
    """
    game_fixture.start_game('easy')
    masked_phrase = game_fixture.get_masked_phrase()
    special_chars = {' ', '\'', '-', '.', ','}
    assert all(c in special_chars for c in masked_phrase if c != '_')


def test_loss_condition(game_fixture: HangmanGame):  # pylint: disable=redefined-outer-name
    """Test the loss condition for exceeding maximum attempts.

    :param HangmanGame game_fixture: An instance of the HangmanGame for testing.
    """
    game_fixture.start_game('easy')
    alphabet = set('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    letters_not_in_phrase = alphabet - set(game_fixture.current_phrase)
    wrong_guesses = 0
    for letter in letters_not_in_phrase:
        if wrong_guesses >= game_fixture.max_attempts:
            break
        result = game_fixture.make_guess(letter)
        if result != 'Letter already guessed.':
            wrong_guesses += 1
    assert game_fixture.get_game_status() == 'lost'


def test_invalid_guess(game_fixture):
    """Test making an invalid guess."""
    game_fixture.start_game('easy')
    invalid_guesses = ['3', 'ab', '!', '']  # examples of invalid guesses
    for guess in invalid_guesses:
        response = game_fixture.make_guess(guess)
        assert response == INVALID_GUESS, f"Failed on guess: {guess}"


def test_get_masked_phrase(game_fixture: HangmanGame):  # pylint: disable=redefined-outer-name
    """Test the get_masked_phrase method for a correctly masked phrase.

    :param HangmanGame game_fixture: An instance of the HangmanGame for testing.
    """
    game_fixture.start_game('easy')
    masked_phrase = game_fixture.get_masked_phrase()
    assert len(masked_phrase) == len(game_fixture.current_phrase)
    assert all(c == '_' or c in game_fixture.guessed_letters or not c.isalpha()
               for c in masked_phrase)


def test_get_game_status(game_fixture: HangmanGame):  # pylint: disable=redefined-outer-name
    """Test the get_game_status method for returning the correct game status.

    :param HangmanGame game_fixture: An instance of the HangmanGame for testing.
    """
    game_fixture.start_game('easy')
    assert game_fixture.get_game_status() == 'in_progress'


def test_api_start_game(api_client_fixture):
    """Test starting a new game using the API with difficulty parameter.

    :param api_client_fixture: A Flask test client for API testing.
    """
    # Include difficulty in the request
    test_difficulty = 'easy'  # or whatever difficulty levels your game supports
    rv = api_client_fixture.post(
        '/start', json={'difficulty': test_difficulty})
    assert rv.status_code == 201
    json_data = rv.get_json()
    assert 'game_id' in json_data
    assert 'status' in json_data
    assert 'masked_phrase' in json_data


def test_api_make_guess(api_client_fixture):
    """Test making a guess in an ongoing game using the API.

    :param api_client_fixture: A Flask test client for API testing.
    """
    # Start a new game first
    test_difficulty = 'easy'  # Replace with an actual difficulty level
    rv_start = api_client_fixture.post(
        '/start', json={'difficulty': test_difficulty})
    assert rv_start.status_code == 201
    game_id = rv_start.get_json()['game_id']

    # Make a guess in the game
    test_guess = 'A'  # Replace with an actual guess
    rv_guess = api_client_fixture.post(
        f'/guess/{game_id}', json={'guess': test_guess})
    assert rv_guess.status_code == 200
    json_data = rv_guess.get_json()
    assert 'status' in json_data
    assert 'masked_phrase' in json_data
    assert 'attempts' in json_data
    assert 'remaining_attempts' in json_data
    assert 'guessed_letters' in json_data


def test_start_game_with_invalid_difficulty(api_client_fixture):
    """Test starting a game with an invalid difficulty level."""
    response = api_client_fixture.post(
        '/start', json={'difficulty': 'invalid'})
    assert response.status_code == 400
    assert 'error' in response.get_json()


def test_make_guess_without_guess_parameter(api_client_fixture):
    """Test making a guess without providing a guess parameter."""
    # Start a game first
    start_response = api_client_fixture.post(
        '/start', json={'difficulty': 'easy'})
    game_id = start_response.get_json()['game_id']

    # Attempt to make a guess without the 'guess' parameter
    guess_response = api_client_fixture.post(f'/guess/{game_id}', json={})
    assert guess_response.status_code == 400
    assert 'error' in guess_response.get_json()
    assert guess_response.get_json()['error'] == 'No guess provided'


def test_make_guess_with_invalid_game_id(api_client_fixture):
    """Test making a guess with an invalid game ID."""
    invalid_game_id = 'nonexistent_id'
    guess_response = api_client_fixture.post(
        f'/guess/{invalid_game_id}', json={'guess': 'A'})
    assert guess_response.status_code == 404
    assert guess_response.get_json() == {'error': 'Game not found'}


def test_get_status_success(api_client_fixture):
    """Test retrieving the status of an existing game."""
    # Start a new game
    start_response = api_client_fixture.post(
        '/start', json={'difficulty': 'easy'})
    game_id = start_response.get_json()['game_id']

    # Retrieve the game status
    status_response = api_client_fixture.get(f'/status/{game_id}')
    assert status_response.status_code == 200
    game_data = status_response.get_json()
    assert 'status' in game_data
    assert 'masked_phrase' in game_data
    assert 'remaining_attempts' in game_data


def test_get_status_invalid_game_id(api_client_fixture):
    """Test retrieving the status with an invalid game ID."""
    invalid_game_id = 'invalid_id'
    status_response = api_client_fixture.get(f'/status/{invalid_game_id}')
    assert status_response.status_code == 404
    assert status_response.get_json() == {'error': 'Game not found'}


def test_direct_400_error(api_client_fixture):
    response = api_client_fixture.get('/test/400')
    assert response.status_code == 400
    assert response.get_json() == {'error': 'Bad request'}


def test_direct_404_error(api_client_fixture):
    response = api_client_fixture.get('/test/404')
    assert response.status_code == 404
    assert response.get_json() == {'error': 'Not found'}
