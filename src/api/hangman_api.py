"""This module provides an API for playing a game of hangman."""

from flask import Flask, jsonify, request, abort
from src.logic.hangman import HangmanGame  # pylint: disable=E0401

app = Flask(__name__)
games = {}  # Dictionary to store game instances, indexed by a game ID


@app.errorhandler(400)
def bad_request(error):
    """Handle 400 Bad Request errors."""
    return jsonify({'error': 'Bad request'}), 400


@app.errorhandler(404)
def not_found(error):
    """Handle 404 Not Found errors."""
    return jsonify({'error': 'Not found'}), 404


@app.route('/test/400')
def test_400():
    abort(400)


@app.route('/test/404')
def test_404():
    abort(404)


@app.route('/start', methods=['POST'])
def start_game() -> tuple:
    """
    Start a new Hangman game and return its details as a JSON object.

    :return: JSON object containing the game ID, status, and masked phrase.
    :rtype: tuple
    """
    data = request.get_json()
    difficulty = data.get('difficulty', None)

    # Add validation for difficulty
    # Adjust this list based on your valid difficulties
    if difficulty not in ['easy', 'medium', 'hard']:
        return jsonify({'error': 'Invalid difficulty level'}), 400

    game = HangmanGame()
    game.start_game(difficulty)
    game_id = str(len(games) + 1)
    games[game_id] = game
    return get_game_json(game_id), 201


@app.route('/guess/<string:game_id>', methods=['POST'])
def make_guess(game_id: str) -> tuple:
    """
    Make a guess in an ongoing Hangman game specified by game_id.

    :param str game_id: The ID of the game.
    :return: JSON object containing the current status, masked phrase, and remaining attempts.
    :rtype: tuple
    """
    game = games.get(game_id)
    if not game:
        return jsonify({'error': 'Game not found'}), 404

    data = request.get_json()
    guess = data.get('guess', None)
    if guess is None:
        return jsonify({'error': 'No guess provided'}), 400

    game.make_guess(guess)

    return get_game_json(game_id), 200


@app.route('/status/<string:game_id>', methods=['GET'])
def get_status(game_id: str) -> tuple:
    """
    Get the current status of a Hangman game specified by game_id.

    :param str game_id: The ID of the game.
    :return: JSON object containing the current status, masked phrase, and remaining attempts.
    :rtype: tuple
    """
    game = games.get(game_id)
    if not game:
        return jsonify({'error': 'Game not found'}), 404

    return get_game_json(game_id), 200


def get_game_json(game_id: str) -> jsonify:
    """
    Get the json details of the game that is passed in.

    :param str game_id: The ID of the game
    :return: JSON object containing game details
    :rtype: jsonify
    """
    game = games[game_id]
    return jsonify({
        'game_id': game_id,
        'status': game.get_game_status(),
        'phrase': game.current_phrase,
        'masked_phrase': game.get_masked_phrase(),
        'attempts': game.attempts,
        'remaining_attempts': game.max_attempts - game.attempts,
        'guessed_letters': list(game.guessed_letters)
    })


if __name__ == '__main__':  # pragma: no cover
    app.run(host='0.0.0.0', port=5000, debug=True)
