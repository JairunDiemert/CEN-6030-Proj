"""
This module contains the HangmanGame class which encapsulates the core logic
for a Hangman game.
"""

import random
from typing import List, Set

# Attempt to import the external phrases at the top level
try:
    from phrases import phrases as external_phrases
except ImportError:
    external_phrases = [
        'Hard Pill to Swallow',
        'Hit Below The Belt',
        'A Cold Day in July',
        'Drive Me Nuts',
        'There\'s No I in Team',
        'Poke Fun At',
        'I Smell a Rat',
        'A Busy Body',
        'Wouldn\'t Harm a Fly',
        'Break The Ice',
        'Don\'t Count Your Chickens Before They Hatch',
        'Drastic Times Call for Drastic Measures',
        'The Bigger They Are, The Harder They Fall',
        'You Can\'t Judge a Book By its Cover'
    ]

# Constants for Game Status and Error Messages
NOT_STARTED = 'not_started'
IN_PROGRESS = 'in_progress'
WON = 'won'
LOST = 'lost'
GUESS_ACCEPTED = 'Guess accepted'

GAME_NOT_IN_PROGRESS = 'Game not in progress.'
INVALID_GUESS = 'Invalid guess. Please enter a single letter.'
LETTER_ALREADY_GUESSED = 'Letter already guessed.'


class HangmanGame:
    """
    A class that encapsulates the core logic for a Hangman game.
    """

    def __init__(self):
        """
        Initialize game variables.
        """
        self.phrases = self.load_phrases()
        self.current_phrase: str = ''
        self.guessed_letters: Set[str] = set()
        self.max_attempts: int = 6
        self.attempts: int = 0
        self.status: str = NOT_STARTED

    @staticmethod
    def load_phrases() -> List[str]:
        """
        Load phrases for the game.
        Returns a list of phrases for the game.
        """
        return external_phrases

    def start_game(self, difficulty: str) -> None:
        """
        Start a new Hangman game.
        """
        if difficulty == 'easy':
            min_length = 0
            max_length = 15
        elif difficulty == 'medium':
            min_length = 15
            max_length = 25
        else:
            min_length = 25
            max_length = 100

        while True:
            self.current_phrase = random.choice(self.phrases).upper()
            if len(self.current_phrase) >= min_length and len(self.current_phrase) < max_length:
                break

        self.guessed_letters.clear()
        self.attempts = 0
        self.status = IN_PROGRESS

    def make_guess(self, letter: str) -> str:
        """
        Make a guess in the current game.
        Validates the guess and updates the game state accordingly.
        Returns a message indicating the result of the guess.
        """
        if self.status != IN_PROGRESS:
            return GAME_NOT_IN_PROGRESS
        if len(letter) != 1 or not letter.isalpha():
            return INVALID_GUESS

        letter = letter.upper()
        if letter in self.guessed_letters:
            return LETTER_ALREADY_GUESSED

        self.guessed_letters.add(letter)
        self.update_game_state(letter)
        return GUESS_ACCEPTED

    def update_game_state(self, letter: str) -> None:
        """
        Update the game state based on the guessed letter.
        """
        if letter not in self.current_phrase:
            self.attempts += 1
            if self.attempts >= self.max_attempts:
                self.status = LOST

        if all(char in self.guessed_letters or not char.isalpha() for char in self.current_phrase):
            self.status = WON

    def get_game_status(self) -> str:
        """
        Get the current status of the game.
        Returns the current status of the game (NOT_STARTED, IN_PROGRESS, WON, LOST).
        """
        return self.status

    def get_masked_phrase(self) -> str:
        """
        Get the current phrase with unguessed letters replaced by underscores.
        Returns the masked phrase.
        """
        special_chars = {' ', '\'', '-', '.', ','}
        return ''.join([
            char if char in self.guessed_letters or char in special_chars
            else '_'
            for char in self.current_phrase
        ])
