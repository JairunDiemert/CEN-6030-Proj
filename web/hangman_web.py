"""
This module is a Flask application for a Hangman game which interfaces with a local API.
"""

import os
import math
import requests
from flask import Flask, render_template, request, url_for, session, flash, redirect

DEFAULT_TIME = 180  # 3 x 60 sec = 180 sec = 3 min
EASY = 'easy'
MEDIUM = 'medium'
HARD = 'hard'
EASY_TIME = 600
MEDIUM_TIME = 300
HARD_TIME = 180

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['DRAWING_FOLDER'] = os.path.join('static', 'img')
app.secret_key = 'your_secret_key_here'  # Required for session to work
app.time_left = EASY_TIME
app.paused = True
app.started = False
app.difficulty = EASY

API_URL = 'http://localhost:5000'  # Replace with your API's URL

# Override if running in a Docker environment
if os.getenv('DOCKER_ENV'):
    API_URL = 'http://hangman-api:5000'  # Docker service name as the host


def start_new_game(difficulty: str):
    """
    Start a new game by making an API call to /start.

    :return: Dictionary containing game data returned by the API.
    :rtype: dict
    """
    res = requests.post(f'{API_URL}/start',
                        json={'difficulty': difficulty}, timeout=5)
    return res.json()


def make_guess(game_id, guess):
    """
    Make a guess in the game identified by game_id.

    :param game_id: Identifier for the game.
    :type game_id: str
    :param guess: The letter being guessed.
    :type guess: str
    :return: Dictionary containing updated game data.
    :rtype: dict
    """
    res = requests.post(f'{API_URL}/guess/{game_id}',
                        json={'guess': guess.lower()}, timeout=5)
    return res.json()


def get_game_status(game_id):
    """
    Retrieve the current status of the game identified by game_id.

    :param game_id: Identifier for the game.
    :type game_id: str
    :return: Dictionary containing current game data.
    :rtype: dict
    """
    res = requests.get(f'{API_URL}/status/{game_id}', timeout=5)
    return res.json()


def start_game(difficulty: str):
    """
    Start a new game with the specified difficulty.

    This function initiates a new game using the start_new_game function.
    It sets the game ID in the session,
    assigns the chosen difficulty to the application,
    and updates the game's state as started and not paused.
    Based on the difficulty level (easy, medium, or hard),
    it sets the corresponding time limit for the game.

    :param difficulty: The difficulty level of the game.
        Expected values are 'easy', 'medium', or 'hard'.
    :type difficulty: str
    :return: Dictionary containing the initial game data
        returned by the start_new_game function.
    :rtype: dict
    """
    game_data = start_new_game(difficulty)
    session['game_id'] = game_data['game_id']
    app.difficulty = difficulty
    app.started = True
    app.paused = False
    if difficulty == EASY:
        app.time_left = EASY_TIME
    elif difficulty == MEDIUM:
        app.time_left = MEDIUM_TIME
    else:
        app.time_left = HARD_TIME
    return game_data


@app.route('/', methods=('GET', 'POST'))
def index():
    """
    Main route handler for the Hangman game.

    Handles both GET and POST requests. If it's a POST request, the user's guess is processed.
    The current game state is displayed.

    :return: Rendered HTML template for the game.
    :rtype: str
    """
    game_data = {}  # Initialize as an empty dictionary
    if 'game_id' in session:
        game_data.update(get_game_status(session['game_id']))

    if request.method == 'POST':
        control_action = request.form.get("Control")
        action_map = {
            "easy": lambda: start_game(EASY),
            "medium": lambda: start_game(MEDIUM),
            "hard": lambda: start_game(HARD),
            "try_again": handle_try_again,
            "give_up": handle_give_up,
            "timeout": handle_timeout,
            "pause": lambda: set_pause('pause'),
            "resume": lambda: set_pause('resume'),
            "default": lambda: handle_guess(request.form.get('Guess', ''))
        }
        response = action_map.get(control_action, action_map['default'])()
        if isinstance(response, dict):
            # Update game data if the response is a dictionary
            game_data.update(response)
        elif response is not None:
            # If the response is not a dictionary and not None, return it (redirects)
            return response
        # No else case needed; if response is None, just continue to render the template

    if 'game_id' in session and "error" not in game_data:
        game_outcome = check_game_outcome(game_data)
        if game_outcome:
            return game_outcome

    phrase, guessed, num = get_template_defaults(game_data)

    return render_template(
        'index.html',
        phrase=phrase,
        guessed=guessed,
        paused=app.paused,
        started=app.started,
        time=format_time(app.time_left),
        drawing=os.path.join(app.config['DRAWING_FOLDER'], f'{num}.png')
    )


@app.route('/time', methods=['GET'])
def time():
    """
    Route which decrements and returns the amount of time left

    :return: String containing the time left
    :rtype: str
    """
    # Decrements the time and returns it unless the time goes below 0
    if not app.paused:
        app.time_left = max(app.time_left - 1, 0)

    return str(abs(math.floor(app.time_left / 60))) + ':' + str(app.time_left % 60).rjust(2, '0')


def handle_try_again():
    """Handle the 'try again' action."""
    flash('Restarting with the same difficulty...')
    session.pop('game_id', None)
    return start_game(app.difficulty)


def handle_give_up():
    """Handle the 'give up' action."""
    game_data = get_game_status(session['game_id'])
    phrase = game_data['phrase']
    flash(f'You gave up! The phrase was "{phrase}".')
    session.pop('game_id', None)
    app.started = False
    app.paused = True
    return redirect(url_for('index'))


def handle_timeout():
    """Handle the 'timeout' action."""
    game_id = session.get('game_id')
    if not game_id:
        app.started = False
        app.paused = True
        return redirect(url_for('index'))

    game_data = get_game_status(session.get('game_id'))
    phrase = game_data['phrase']
    flash(f'You ran out of time! The phrase was "{phrase}".')
    session.pop('game_id', None)
    app.started = False
    app.paused = True
    return redirect(url_for('index'))


def set_pause(action: str):
    """
    Set the pause status of the game based on the provided action.

    This function updates the 'paused' state of the application depending on the action.

    :param action: The action received from the POST request.
                   Expected values are 'pause' or 'resume'.
    :type action: str
    :return: None. This function does not return a value but alters the state of the application.
    """
    if action == "pause":
        app.paused = True
    elif action == "resume":
        app.paused = False


def handle_guess(guess):
    """Process the user's guess."""
    return make_guess(session['game_id'], guess)


def check_game_outcome(game_data):
    """Check if the game is won or lost and restart if so."""
    if game_data['status'] in ['won', 'lost']:
        flash(
            f'Game Over! You {game_data["status"]}. '
            f'The phrase was {game_data["phrase"]} -- '
            f'your score was {str(get_score())}'
        )
        session.pop('game_id', None)
        app.started = False
        app.paused = True
        return redirect(url_for('index'))
    return None


def get_template_defaults(game_data):
    """
    Get default values for template rendering.

    :param game_data: The current game data.
    :type game_data: dict
    :return: Tuple containing phrase, guessed letters, and attempt number.
    :rtype: tuple
    """
    if game_data:
        phrase = game_data.get('masked_phrase', '')
        wrong_guesses = [x for x in game_data.get(
            'guessed_letters', []) if x not in game_data.get('phrase', '')]
        guessed = ', '.join(wrong_guesses).upper()
        num = game_data.get('attempts', 0) + 1
    else:
        phrase, guessed, num = "", "", 1

    return phrase, guessed, num


def format_time(time_left):
    """Format time for display."""
    minutes = abs(math.floor(time_left / 60))
    seconds = time_left % 60
    return f'{minutes}:{str(seconds).rjust(2, "0")}'


def get_score() -> float:
    game_data = get_game_status(session['game_id'])
    phrase = len(game_data['phrase'])
    wrong_guesses = len(game_data['guessed_letters'])
    seconds = sum(x * int(t) for x, t in zip([1, 60, 3600], reversed(time().split(":"))))
    
    if app.difficulty == "easy":
        time_dif = EASY_TIME - seconds
        score = (time_dif * 0.5) + (phrase * 1) -(wrong_guesses * 1)
    elif app.difficulty == "medium":
        time_dif = MEDIUM_TIME - seconds
        score = (time_dif * 0.6) + (phrase * 1.5) - (wrong_guesses * 1)
    elif app.difficulty == "hard":
        time_dif = HARD_TIME - seconds
        score = (time_dif * 0.7) + (phrase * 2) - (wrong_guesses * 1)

    if game_data["status"] == "lost":
        score = 0

    return score
       

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4000, debug=True)
