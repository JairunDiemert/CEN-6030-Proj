"""
This is the Hangman CLI module. It provides a command line interface for playing a game of Hangman.
"""

from src.logic.hangman import HangmanGame, IN_PROGRESS, WON, LOST  # pylint: disable=import-error

# ANSI Color Codes as Named Constants
GREEN_BOLD = '92;1'
YELLOW = '93'
RED = '91'
MAGENTA = '95'
BLUE = '94'


def get_user_guess(prompt: str) -> str:
    """
    Capture user input for making a guess.

    :param str prompt: The prompt to display before capturing the input.
    :return: The captured input from the user.
    :rtype: str
    """
    return input(prompt)


def print_colored_message(message: str, color_code: str) -> None:
    """
    Print a message to the console with ANSI color coding.

    :param str message: The message to print.
    :param str color_code: The ANSI color code to use for the message.
    """
    print(f'\033[{color_code}m{message}\033[0m')


def display_game_status(game: HangmanGame, print_message) -> None:
    """
    Display the current status of the Hangman game.

    :param HangmanGame game: An instance of the HangmanGame class.
    :param function print_message: A function for printing colored messages.
    """
    print_message(f'\nPhrase to guess: {game.get_masked_phrase()}', YELLOW)
    print_message(
        f'\tGuessed letters: {", ".join(game.guessed_letters)}', MAGENTA)
    print_message(
        f'\tRemaining attempts: {game.max_attempts - game.attempts}\n', BLUE)


def run_hangman_cli(get_user_input=get_user_guess, print_message=print_colored_message) -> None:
    """
    Run the Hangman game using a Command Line Interface.

    :param function get_user_input: Function to capture user input.
     Defaults to get_user_guess.
    :param function print_message: Function to print colored messages.
     Defaults to print_colored_message.
    """
    game = HangmanGame()
    game.start_game('easy')

    print_message('\nWelcome to Hangman CLI!\n', GREEN_BOLD)
    display_game_status(game, print_message)

    while game.get_game_status() == IN_PROGRESS:
        guess = get_user_input('Enter your guess (single letter): ')
        feedback = game.make_guess(guess)

        if feedback:
            print_message(f'\t{feedback}', RED)

        display_game_status(game, print_message)

        if game.get_game_status() == WON:
            print_message('Congratulations, you won!', GREEN_BOLD)
            break
        if game.get_game_status() == LOST:
            print_message('You lost. Better luck next time!', RED)
            print_message(f'The phrase was: {game.current_phrase}', RED)
            break


if __name__ == '__main__':
    run_hangman_cli()
