# Directories
DIR_DOCS	:= docs
DIR_ROOT := $(shell pwd)

# Determine OS type: macOS (Darwin), Linux, or Windows (WSL)
UNAME := $(shell uname)

.PHONY: help build docs clean test install startCLI startAPI startWeb start stop openWeb

# Display usage information
help:
	@echo "Usage:"
	@echo "  make build            Build the project (install dependencies, generate docs, run tests)"
	@echo "  make install          Install Python dependencies"
	@echo "  make docs             Generate Sphinx documentation"
	@echo "  make test             Run Pytest tests"
	@echo "  make clean            Clean up generated files and logs"
	@echo "  make startCLI         Start the Hangman CLI"
	@echo "  make startAPI         Start the Hangman API server"
	@echo "  make startWeb         Start the Hangman Web server"
	@echo "  make openWeb          Open the Web server in the default browser"
	@echo "  make start            Start both the Hangman API and Web servers"
	@echo "  make stop             Stop any running Hangman servers"

# Complete build including dependencies, docs, and tests
build: install docs test

# Install Python dependencies
install:
	@pip install -r requirements.txt

# Generate Sphinx documentation
docs:
	@$(MAKE) -C $(DIR_DOCS) updateReadme
	@$(MAKE) -C $(DIR_DOCS) html

# Run Pytest
test:
	@pytest $(DIR_ROOT) --cov --cov-report term --cov-report xml:test_reports/coverage.xml --junitxml=test_reports/report.xml

# Clean up generated files, cache, logs, documentation, and test reports
clean:
	@$(MAKE) -C $(DIR_DOCS) clean
	@find . -type d -name "__pycache__" -exec rm -rf {} +
	@find . -type d -name ".pytest_cache" -exec rm -rf {} +
	@rm -rf $(DIR_ROOT)/server_logs/
	@rm -rf $(DIR_ROOT)/docs/build/
	@rm -rf $(DIR_ROOT)/test_reports/

# Prepare logs directory
prepareLogs:
	@mkdir -p $(DIR_ROOT)/server_logs

# Open the Web server in the default browser
openWeb:
	@UNAME=$$(uname); \
	WSL=$$(grep -qEi 'microsoft|WSL' /proc/version 2>/dev/null && echo 'true' || echo 'false'); \
	if [ "$$UNAME" = 'Darwin' ]; then open http://localhost:4000 || echo 'Could not open browser. Visit http://localhost:4000 manually.'; \
	elif [ "$$UNAME" = 'Linux' ]; then \
		if [ "$$WSL" = 'true' ]; then cmd.exe /C 'start http://localhost:4000' || echo 'Could not open browser. Visit http://localhost:4000 manually.'; \
		else xdg-open http://localhost:4000 || echo 'Could not open browser. Visit http://localhost:4000 manually.'; \
		fi; \
	else cmd.exe /C 'start http://localhost:4000' || echo 'Could not open browser. Visit http://localhost:4000 manually.'; \
	fi

# Start the Hangman CLI
startCLI:
	@cd $(DIR_ROOT) && python -m cli.hangman_cli

# Start the Hangman API server
startAPI: prepareLogs
	@cd $(DIR_ROOT) && echo "Service started on `date`" > server_logs/api.log
	@cd $(DIR_ROOT) && nohup python -m src.api.hangman_api >> server_logs/api.log 2>&1 &

# Start the Hangman Web server
startWeb: prepareLogs
	@cd $(DIR_ROOT) && echo "Service started on `date`" > server_logs/web.log
	@cd $(DIR_ROOT) && nohup python -m web.hangman_web >> server_logs/web.log 2>&1 &
	@$(MAKE) openWeb

# Start both Hangman servers
start : startAPI startWeb

# Stop any running servers
stop:
	pkill -f 'hangman' || true

