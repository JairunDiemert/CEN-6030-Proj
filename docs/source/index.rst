.. Hangman documentation master file, created by
   sphinx-quickstart on Tue Oct 24 18:32:21 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Hangman App
===================================

.. toctree::
   :maxdepth: 2

   ./pages/README.md
   ./pages/api_logic.rst
   ./pages/core_logic.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
