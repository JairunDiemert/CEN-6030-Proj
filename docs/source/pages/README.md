
# CEN-6030-Proj: Hangman Game in Python

This repository contains a group project for CEN 6030. The project implements the Hangman game in Python, offering three different interfaces: Command Line Interface (CLI), Application Programming Interface (API), and a Web-based interface.

---

## Table of Contents

- [Game Rules](#game-rules)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Optional: Using Makefile](#optional-using-makefile)
- [Folder Structure](#folder-structure)
- [How to Play (CLI Version)](#how-to-play-cli-version)
- [How to Play (API Version)](#how-to-play-api-version)
- [How to Play (Web Version)](#how-to-play-web-version)
- [How to Run Tests](#how-to-run-tests)
- [Implementation Details](#implementation-details)
- [Special Features](#special-features)

---

## Game Rules

Hangman is a classic word-guessing game. In this version, the player has 6 attempts to guess an entire phrase by suggesting letters one at a time.

- **Win Condition**: Guess the entire phrase before running out of attempts.
- **Loss Condition**: Run out of attempts without guessing the phrase.

Special characters, such as apostrophes, hyphens, and spaces, are automatically revealed.

---

## Prerequisites

    # Auto-Documentation
    sphinx>=7.0,<8.0
    sphinx-rtd-theme==1.3.0
    docutils<0.19
    myst-parser==2.0.0

    # Web framework for API
    Flask==2.3.3
    requests==2.26.0

    # Testing framework
    pytest==7.4.2
    pytest-cov==4.1.0

    # Containerized deployment
    docker>=20.10.8,<21.0.0
    docker-compose>=1.29.2,<2.2.0

---

## Installation

### Manual Installation

1. Clone the repository.
2. Navigate to the project directory.
3. Install required packages using pip.

```bash
pip install -r requirements.txt
```

### Using Make (Optional)

Alternatively, you can run the following `make` command to install the project dependencies:

```bash
make install
----OR-----
make build
```

---

## Folder Structure

```
/hangman_project
├── Makefile
├── README.md
├── cli
├── docker-compose.yml
├── docs
├── hangman_test.py
├── requirements.txt
├── src
│   ├── api
│   └── logic
├── start.sh
├── stop.sh
└── web
```

---

## Optional: Using Makefile

For a more streamlined development workflow, you can utilize the `Makefile` included in this project. Here are some useful `make` commands:

- `make build`: Builds the project, installs dependencies, generates documentation, and runs tests.
- `make install`: Installs Python dependencies.
- `make docs`: Generates Sphinx documentation.
- `make test`: Runs Pytest tests.
- `make startCLI`: Starts the Hangman CLI.
- `make startAPI`: Starts the Hangman API server.
- `make startWeb`: Starts the Hangman Web server.
- `make openWeb`: Opens the Hangman Web server in your default browser.
- `make start`: Starts both the Hangman API and Web servers.
- `make stop`: Stops any running Hangman servers.

---

## General Deployment Instructions

### Local Deployment

1. Navigate to the `root` directory.
2. Run the command:

### START
```bash
make start
```
### STOP
```bash
make stop
```

### Using Docker Compose (Optional)

1. Navigate to the `root` directory.
2. Run the command:

### START
```bash
./start.sh
------OR-------
docker-compose up -d
```

### STOP
```bash
./stop.sh
------OR-------
docker-compose down
```

# [Play the Game](http://localhost:4000 "Link to Game Page in Browser (Local) http://localhost:4000")


## How to Play (CLI Version)

### Manual Initiation

1. Navigate to the `root` directory.
2. Run the command:

```bash
python -m src.api.hangman_cli
```

Follow the on-screen instructions.

### Using Make (Optional)

Alternatively, you can run the following `make` command to start the game:

```bash
make startCLI
```
Follow the on-screen instructions.

---

## How to Play (API Version)

### Manual Initiation

1. Navigate to the `root` directory.
2. Run the command:

```bash
python -m src.api.hangman_api
```

### Using Make (Optional)

Alternatively, you can run the following `make` command to start the API server:

```bash
make startAPI
```

This will start the API server. To interact with it, use the defined endpoints.

---

## How to Play (Web Version)

### Manual Initiation

1. Navigate to the `root` directory.
2. Run the command:

```bash
python -m web.hangman_web
```


### Using Make (Optional)

Alternatively, you can run the following `make` command to start the Web server:

```bash
make startWeb
```

Then, open your web browser and go to `http://localhost:4000`.

### Using Docker Compose (Optional)

For those not as familiar with Python or who prefer using Docker, the game can also be run using Docker Compose.

1. Ensure Docker and Docker Compose are installed on your system.
2. Navigate to the `root` directory.
3. Run the following command to start the services:

```bash
./start.sh
```

The script will check for Docker Compose, start the necessary services, and open your default web browser to `http://localhost:4000`. If the browser does not open automatically, you can manually navigate to this URL.

To stop the services, run:

```bash
./stop.sh
```

---

## How to Run Tests

### Manual Initiation

To run tests, navigate to the `root` directory and run:

```bash
pytest
```

### Using Make (Optional)

Alternatively, you can run the following `make` command to run the tests:

```bash
make test
```

---

## Implementation Details

- Core logic is encapsulated in the `HangmanGame` class (`src/logic/hangman.py`).
- CLI is implemented in `hangman_cli.py`.
- API is implemented in `src/api/hangman_api.py`.
- Web interface is implemented in `web/hangman_web.py`.

---

## Special Features

- Supports phrases, not just single words.
- Special characters are automatically revealed.
- Colored text in CLI for improved user experience.
- API and Web interfaces for remote play.
- API and Web interfaces support multiple concurrent games.
- Dockerized deployment for easy setup.
- Sphinx documentation for easy reference.
- Pytest tests for easy verification.
- Makefile for easy development.
- Modular design for easy extensibility.
- Extensive error handling for improved robustness.

